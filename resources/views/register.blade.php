<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="first_name">First name : </label><br><br>
        <input type="text" name="first_name"><br><br>

        <label for="last_name">Last name : </label><br><br>
        <input type="text" name="last_name"><br><br>

        <p>Gender</p>
        <input type="radio" name="gender" id="male" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="Female">
        <label for="female">Female</label><br><br>

        <label for="nationality">Nationality</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select><br>

        <p>Language Spoken</p>
        <input type="checkbox" name="language" value="indonesia" id="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" value="english" id="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" value="other" id="other">
        <label for="other">Other</label><br><br>

        <label for="bio">Bio</label><br><br>
        <textarea id="bio" name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up"><br>

    </form>
</body>
</html>
