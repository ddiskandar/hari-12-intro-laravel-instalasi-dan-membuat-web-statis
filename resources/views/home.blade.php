<!DOCTYPE html>
<html>
<head>
    <title>Index</title>
</head>
<body>
    <h1>SanberBook</h1>

    <h3>Sosial Media Developer Santai Berkualitas</h3>
    <p>Belajar dan berbagi agar hidup menjadi semakin santai berkualitas</p>

    <h3>Benefit Join di Sanberbook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowlenge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h3>Cara Bergabung ke Sanberbook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="{{ route('register') }}">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
</body>
</html>
