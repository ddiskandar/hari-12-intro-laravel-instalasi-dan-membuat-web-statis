<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@home')->name('home');
Route::get('register', 'AuthController@register')->name('register');
Route::post('welcome', 'AuthController@welcome');
